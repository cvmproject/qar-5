local cameraPos = Vector(0, 0)
local cameraViewSize = Vector(100, 100)
local lastUpdate = 0

local maxDistanceUntilSnap = 2048
local maxSizeDifferenceUntilSnap = 2048

camera_pos = Vector(100, 100)

hook.DefineEvent("CameraFocusUpdate")

hook.Add("CameraSetup", "CamUpdatePos", function()
    local deltaT = CurTime() - lastUpdate
    if lastUpdate == 0 then deltaT = 0 end
    lastUpdate = CurTime()

    local active_controller = nil
    for id, camera_controller in ipairs(ents.GetByType("camera_controller")) do
        if camera_controller:GetAlwaysActive() or camera_controller:GetCollider():Contains(camera_pos) then
            active_controller = camera_controller
            break
        end
    end


    hook.CallEvent("CameraFocusUpdate")
    local targetCameraPos = camera_pos
    local targetCameraViewSize = Vector(800, 600)
    if active_controller ~= nil then
        local sizeX = 800
        local sizeY = 600
        local lockX = active_controller:GetLockX()
        local lockY = active_controller:GetLockY()

        local posX = camera_pos.x
        local posY = camera_pos.y

        if lockX then
            sizeX = active_controller:GetSizeX()
            posX = active_controller:GetPos().x + sizeX/2
        end

        if lockY then
            sizeY = active_controller:GetSizeY()
            posY = active_controller:GetPos().x + sizeY/2
        end
        targetCameraViewSize.x = sizeX
        targetCameraViewSize.y = sizeY
        
        targetCameraPos.x = posX
        targetCameraPos.y = posY
    end

    local viewSizeChange = (targetCameraViewSize - cameraViewSize)/10
    local posChange = (targetCameraPos - cameraPos)/10

    if viewSizeChange:Length() > 1 then
        viewSizeChange:Square()
    end

    if posChange:Length() > 1 then
        posChange:Square()
    end

    if viewSizeChange:Length() < 20 then
        viewSizeChange = viewSizeChange/viewSizeChange:Length()*20
    end

    if posChange:Length() < 20 then
        posChange = posChange/posChange:Length()*20
    end

    if viewSizeChange:Length() > 10000 then
        viewSizeChange:Mul(10000/viewSizeChange:Length())
    end

    if posChange:Length() > 10000 then
        posChange:Mul(10000/posChange:Length())
    end

    viewSizeChange:Mul(deltaT)
    posChange:Mul(deltaT)


    if (targetCameraViewSize - cameraViewSize):Length() < 0.1 or (targetCameraViewSize - cameraViewSize):Length() > maxSizeDifferenceUntilSnap then
        viewSizeChange = (targetCameraViewSize - cameraViewSize)
    end

    if (targetCameraPos - cameraPos):Length() < 0.1 or (targetCameraPos - cameraPos):Length() > maxDistanceUntilSnap then
        posChange = (targetCameraPos - cameraPos)
    end

    cameraViewSize:Add(viewSizeChange)
    cameraPos:Add(posChange)

    cam.SetPos(cameraPos)
    cam.SetViewSize(cameraViewSize.x, cameraViewSize.y)
end)


function SetMaxDistanceUntilSnap(value)
    maxDistanceUntilSnap = value
end

function SetMaxSizeDifferenceUntilSnap(value)
    maxSizeDifferenceUntilSnap = value
end

function SetCameraFocus(value)
    camera_pos = value
end

console.AddCommand("camera_controller_get_focus", function()
    print(camera_pos.x..":"..camera_pos.y)
end)

console.AddCommand("camera_controller_get_active", function()

end)
