function ENT:SetupDataTable()
    self:AddDataTableEntry("SizeX", "Int", 100)
    self:AddDataTableEntry("SizeY", "Int", 100)
    self:AddDataTableEntry("LockX", "Bool", false)
    self:AddDataTableEntry("LockY", "Bool", false)
    self:AddDataTableEntry("AlwaysActive", "Bool", false)
end

function ENT:GetCollider()
    if self.collider == nil then
        local pos = self:GetPos()
        self.collider = rectangle.Rectangle(pos, Vector(self:GetSizeX(), self:GetSizeY()))
    end

    return self.collider
end

function ENT:DrawDebug()
    draw.SetColor(Color(255, 255, 0, 100))
    draw.Rect(self:GetPos().x, self:GetPos().y, self:GetSizeX(), self:GetSizeY())
end
