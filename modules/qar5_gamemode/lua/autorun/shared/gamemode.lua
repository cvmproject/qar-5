local gamemodes = {}
local active_gamemode = nil

local maps = {}

function RegisterGamemode(gamemode)
    if gamemode.name == nil then error("Attempted to register a gamemode with no name!", 2) end
    if gamemode.id == nil then error("Attempted to register a gamemode with no id!", 2) end
    if gamemode.on_load == nil then error("Attempted to register a gamemode with no on_load function!", 2) end

    if gamemodes[gamemode.id] ~= nil then error("Attempted to register an existing gamemode id "..gamemode.id, 2) end
    print("Registering gamemode "..gamemode.name)
    gamemode.available_maps = {}
    gamemodes[#gamemodes + 1] = gamemode
    gamemodes[gamemode.id] = gamemode
end

function ListGamemodes()
    -- TODO: Should return a copy
    return gamemodes
end

function LoadGamemode(gamemode_id)
    local gamemode = gamemodes[gamemode_id]
    if gamemode == nil then error("Attempted to load an unknown gamemode with id "..gamemode_id, 2) end
    if active_gamemode ~= nil then error("Attempted to load a gamemode while another one is active!", 2) end

    active_gamemode = gamemode
    print("Loading gamemode "..gamemode.name)
    gamemode.on_load()
end

function GetActiveGamemode() return active_gamemode end

function RegisterMap(map)
    if map.name == nil then error("Attempted to register a map with no name", 2) end
    if map.gamemodes == nil or #map.gamemodes == 0 then error("Attempted to register a map with no gamemodes you can play it in", 2) end
    if map.path == nil then error("Attempted to register a map with no path", 2) end

    for gamemode_id in ipairs(map.gamemodes) do
        if gamemodes[gamemode_id] == nil then error("Attempted to register a map for an unknown gamemode id "..gamemode_id, 2) end
        gamemodes[gamemode_id].available_maps[#gamemodes[gamemode_id].available_maps + 1] = map
    end

    print("Registering map "..map.name)

    maps[#maps + 1] = map
end

function ListAllMaps() return maps end

function ListMapsForGamemode(gamemode_id)
    if gamemodes[gamemode_id] == nil then error("Attempted to list maps for unknown gamemode id "..gamemode_id, 2) end
    return gamemodes[gamemode_id].available_maps
end


if SERVER then
    hook.Add("ModulesLoaded", "LoadGamemode", function()
        local gamemode_id = server.GetServerParams().gamemode
        if gamemode_id == nil then return end

        LoadGamemode(gamemode_id)
    end)
end
