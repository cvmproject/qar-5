local match_over = false

qar5_gamemode.RegisterGamemode({
    name="Deathmatch",
    id="dm",
    on_load=function()
        hook.Add("Qar5CharacterDeath", "IncreaseKillerScore", function(_, killer)
            if not match_over and killer ~= nil then
                killer:GetDataEntity():SetScore(killer:GetDataEntity():GetScore() + 1)
                if killer:GetDataEntity():GetScore() >= 2 then
                    hook.Call("Qar5MatchEnd")
                end
            end
        end)
    end
})


qar5_gamemode.RegisterMap({
    path="dm_reload_arena",
    name="Q1R Arena",
    gamemodes={"dm"}
})

qar5_gamemode.RegisterMap({
    path="dm_elevator",
    name="Elevator",
    gamemodes={"dm"}
})
