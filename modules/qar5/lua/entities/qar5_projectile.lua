PROJECTILE_SIZE = 16

function ENT:Init()
    if CLIENT then
        self.last_particle_spawn = CurTime()
        --self:SetPredicted(true)
    end
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("Owner", "Player", nil)
    self:AddDataTableEntry("Color", "Color", Color(255, 255, 255))
end

function ENT:Tick(delta)
    if CLIENT then
        if CurTime() > self.last_particle_spawn + 0.02 then
            self.last_particle_spawn = CurTime()

            local follower_particle = ents.Create("qar5_particle")
            follower_particle:SetPos(self:GetPos())
            follower_particle.color = self:GetColor()
            ents.SpawnClientside(follower_particle)

            for i = 1, 3 do
                local sprinkle_particle = ents.Create("qar5_particle")
                sprinkle_particle:SetPos(self:GetPos())
                sprinkle_particle:SetVel(Vector(math.random()*200-100, math.random()*200-100))
                sprinkle_particle.color = self:GetColor()
                ents.SpawnClientside(sprinkle_particle)
            end
        end
        --return
    end
    if IsRemote() then return end
    for id, entity in pairs(ents.GetAllActive()) do
        if entity:GetType() == "qar5_platform" then
            local platform_pos = entity:GetPos()
            local platform_size = Vector(entity:GetSizeX(), entity:GetSizeY())
            local platformRect = rectangle.Rectangle(platform_pos, platform_size)
            if self:GetColliderWhole():Intersects(platformRect) then
                local effect = ents.Create("qar5_effect_hitwall")
                effect:SetPos(self:GetPos())
                effect:SetColor(self:GetColor())
                ents.Spawn(effect)
                self:Remove()
            end
        end
    end
    if not rectangle.Contains(Vector(-10000, -10000), Vector(20000, 20000), self:GetPos()) then self:Remove() end
    for id, entity in pairs(ents.GetAllActive()) do
        if entity:GetType() == "qar5_character" then
            if entity:GetOwner() == self:GetOwner() then goto continue end
            if self:GetColliderWhole():Intersects(entity:GetColliderWhole()) then
                local effect = ents.Create("qar5_effect_hitwall")
                effect:SetPos(self:GetPos())
                effect:SetHitCharacter(entity)
                effect:SetColor(self:GetColor())
                ents.Spawn(effect)

                if self:GetPos().x < entity:GetPos().x then
                    entity:GetHit(false, 1, self:GetOwner())
                else
                    entity:GetHit(true, 1, self:GetOwner())
                end
                self:Remove()
            end
        end
        :: continue ::
    end
end

function ENT:GetColliderWhole()
    newPos = self:GetPos()
    return rectangle.Rectangle(Vector(newPos.x-PROJECTILE_SIZE/2, newPos.y - PROJECTILE_SIZE/2), Vector(PROJECTILE_SIZE, PROJECTILE_SIZE))
end


function ENT:Draw()
    local pos = self:GetPos()
    
    draw.SetColor(self:GetColor())
    draw.Rect(pos.x - PROJECTILE_SIZE/2, pos.y - PROJECTILE_SIZE/2, PROJECTILE_SIZE, PROJECTILE_SIZE)
end

function ENT:Freeze()
    self:Remove()
    return true
end