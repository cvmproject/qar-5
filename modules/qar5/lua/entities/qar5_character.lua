if CLIENT then
    PLAYER_IMAGE_PREFIX = "players/player"
end
PLAYER_SIZE = 32

function ENT:Init()
    self:SetAutoPhysicsEnabled(false)

    self.jumping = false
    self.jumps_left = 1

    self.bot_direction = 0
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("Owner", "Player", nil)
    self:AddDataTableEntry("Direction", "Int", 1)
    self:AddDataTableEntry("InControl", "Int", 1)
    self:AddDataTableEntry("ExplodingTime", "Float", 0)
    self:AddDataTableEntry("Health", "Int", 4)
    self:AddDataTableEntry("NextFireMilli", "Int", CurTime()*1000)
end

function ENT:GetColliderBottom(newPos)
    newPos = newPos or self:GetPos()
    return rectangle.Rectangle(Vector(newPos.x - PLAYER_SIZE/2, newPos.y - PLAYER_SIZE/2), Vector(PLAYER_SIZE, PLAYER_SIZE/2))
end

function ENT:GetColliderTop(newPos)
    newPos = newPos or self:GetPos()
    return rectangle.Rectangle(Vector(newPos.x - PLAYER_SIZE/2, newPos.y), Vector(PLAYER_SIZE, PLAYER_SIZE/2))
end

function ENT:GetColliderLeft(newPos)
    newPos = newPos or self:GetPos()
    return rectangle.Rectangle(Vector(newPos.x - PLAYER_SIZE/2, newPos.y - PLAYER_SIZE/2), Vector(PLAYER_SIZE/2, PLAYER_SIZE))
end

function ENT:GetColliderRight(newPos)
    newPos = newPos or self:GetPos()
    return rectangle.Rectangle(Vector(newPos.x, newPos.y - PLAYER_SIZE/2), Vector(PLAYER_SIZE/2, PLAYER_SIZE))
end

function ENT:GetColliderWhole()
    newPos = self:GetPos()
    return rectangle.Rectangle(Vector(newPos.x-PLAYER_SIZE/2, newPos.y - PLAYER_SIZE/2), Vector(PLAYER_SIZE, PLAYER_SIZE))
end

function ENT:Tick(delta)
    local owner = self:GetOwner()

    if owner == nil and not self:GetIsBot() then return end

    if CLIENT and self:GetExplodingTime() ~= 0 then
        for i = 1, 3 do
            local sprinkle_particle = ents.Create("qar5_particle")
            sprinkle_particle:SetPos(self:GetPos())
            sprinkle_particle:SetVel(Vector(math.random()*400-200, math.random()*400-200))
            sprinkle_particle.color = owner:GetDataEntity():GetColor()
            ents.SpawnClientside(sprinkle_particle)
        end
    end

    if IsRemote() then return end
    

    local input_jump = false
    local input_fire = false
    local input_left = false
    local input_right = false

    if owner ~= nil then
        input_jump = owner:GetInput("qar5_jump")
        input_fire = owner:GetInput("qar5_fire")
        input_left = owner:GetInput("qar5_left")
        input_right = owner:GetInput("qar5_right")
    end

    if self:GetExplodingTime() > 0 then
        input_left = false
        input_right = false
        input_jump = false
        input_fire = false
    end

    local vel = self:GetVel()
    if input_jump and self.jumps_left > 0 then
        if vel.y > 0 then
            vel.y = vel.y + 750
        else
            vel.y = 750
        end
        self.jumps_left = self.jumps_left - 1
    end

    local horizontal_movement = 0
    if input_right then
        self:SetDirection(1)
        horizontal_movement = 500
    elseif input_left then
        self:SetDirection(-1)
        horizontal_movement = -500
    else
        horizontal_movement = 0
    end
    if self:GetInControl() == 1 then
        vel.x = horizontal_movement
    else
        vel.x = vel.x + horizontal_movement*2*delta
    end
    self:SetVel(vel)



    -- Gravity
    if self:GetExplodingTime() == 0 then
        self:SetVel(self:GetVel() - Vector(0, 2000*delta))
    else
        self:SetVel(self:GetVel() - Vector(0, 1000*delta))
    end

    local oldPos = self:GetPos()
    local newVel = self:GetVel()

    local newPos = self:GetPos() + self:GetVel()*delta

    local leftCollider = 0
    local rightCollider = 0
    local topCollider = 0
    local bottomCollider = 0

    function UpdateColliders()
        leftCollider = self:GetColliderLeft(newPos)
        rightCollider = self:GetColliderRight(newPos)
        topCollider = self:GetColliderTop(newPos)
        bottomCollider = self:GetColliderBottom(newPos)
    end

    UpdateColliders()

    for id, entity in pairs(ents.GetAllActive()) do
        if entity:GetType() ~= "qar5_platform" then goto continue end
        local platform_pos = entity:GetPos()
        local platform_size = Vector(entity:GetSizeX(), entity:GetSizeY())
        local platformRect = rectangle.Rectangle(platform_pos, platform_size)

        -- Horizontal Collision
        if newVel.x > 0 and platformRect:Intersects(rightCollider) and not platformRect:Intersects(leftCollider) then
            newVel.x = 0
            newPos.x = platformRect.pos.x - PLAYER_SIZE/2
            UpdateColliders()
        elseif newVel.x < 0 and platformRect:Intersects(leftCollider) and not platformRect:Intersects(rightCollider) then
            newVel.x = 0
            newPos.x = platformRect.pos.x + platformRect.size.x + PLAYER_SIZE/2
            UpdateColliders()
        end

        -- Vertical Collision
        if platformRect:Intersects(bottomCollider) and not platformRect:Intersects(topCollider) then
            newVel.y = entity:GetVel().y
            newVel.x = newVel.x + entity:GetVel().x
            newPos.y = platformRect.pos.y + platformRect.size.y + PLAYER_SIZE/2
            newPos.x = newPos.x + delta*entity:GetVel().x
            self.jumps_left = 1
            self:SetInControl(1)
            UpdateColliders()
        elseif platformRect:Intersects(topCollider) and not platformRect:Intersects(bottomCollider) then
            newVel.y = 0
            newPos.y = platformRect.pos.y - PLAYER_SIZE/2
            UpdateColliders()
        end
        ::continue::
    end

    self:SetVel(newVel)
    self:SetPos(newPos)

    if input_fire and CurTime()*1000 > self:GetNextFireMilli() then
        --owner:EnterLagCompensation()
        self:SetNextFireMilli(CurTime()*1000 + 1000)

        local effect = ents.Create("qar5_effect_fire")
        effect:SetPos(self:GetPos())
        effect:SetDirection(self:GetDirection())
        effect:SetVel(self:GetVel())
        -- effect:SetColor(self:GetOwner():GetDataEntity():GetColor())
        ents.Spawn(effect)
        local projectile = ents.Create("qar5_projectile")
        projectile:SetPos(self:GetPos())
        projectile:SetVel(self:GetVel() + Vector(600, 0)*self:GetDirection())
        projectile:SetOwner(self:GetOwner())
        projectile:SetColor(self:GetOwner():GetDataEntity():GetColor())
        ents.Spawn(projectile)
        --if CLIENT then

        --end
        --owner:ExitLagCompensation()
    end

    if self:GetExplodingTime() ~= 0 and CurTime() >= self:GetExplodingTime() then
        self:SetExplodingTime(0)
        hook.Call("Qar5CharacterDestroyed")
        self:Remove()
        timer.Simple(
            2,
            function() qar5.SpawnCharacterFor(self:GetOwner()) end
        )
    end

    if self:GetPos().y < -10000 then
        -- Probably fell from the map
        self:GetHit(false, 1, nil)
    end
end

function ENT:Freeze()
    return false
end

function ENT:GetHit(left, damage, attacker)
    hook.Call("Qar5TakeDamage", self, 1, attacker)

    self:SetInControl(0)
    gameController:EmitSound("effects/hit_1")
    local vel = self:GetVel()
    if left then vel.x = -400 else vel.x = 400 end
    vel.y = 800
    self:SetVel(vel)

    self:SetHealth(self:GetHealth() - damage)
    if self:GetHealth() <= 0 then
        hook.Call("Qar5CharacterDeath", self, attacker)
        self:BeginExploding()
    end
end

function ENT:BeginExploding()
    self:EmitSound("effects/shatter_05s")
    self:SetExplodingTime(CurTime() + 0.5)
end

function ENT:Draw()
    local pos = self:GetPos()
    
    local img_file = PLAYER_IMAGE_PREFIX
    if self:GetDirection() == -1 then
        img_file = PLAYER_IMAGE_PREFIX.."_left"
    end
    draw.SetColor(self:GetOwner():GetDataEntity():GetColor())
    draw.Rect(pos.x - PLAYER_SIZE/2, pos.y - PLAYER_SIZE/2, PLAYER_SIZE, PLAYER_SIZE)
    draw.Sprite(img_file, pos.x - PLAYER_SIZE/2, pos.y - PLAYER_SIZE/2, PLAYER_SIZE, PLAYER_SIZE)
end
