function ENT:Init()
    if IsRemote() then return end
    self.next_fire = CurTime() + 2
    self:SetPos(Vector(600, 20))
end

function ENT:Tick()
    if IsRemote() then return end
    if CurTime() > self.next_fire then
        self.next_fire = CurTime() + 2
        local projectile = ents.Create("qar5_projectile")
        projectile:SetPos(self:GetPos())
        projectile:SetVel(self:GetVel() - Vector(600, 0))
        ents.Spawn(projectile, 1)
    end
end
