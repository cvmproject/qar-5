if CLIENT then
    PLATFORM_COLOR = Color(255, 255, 255)
end

function ENT:Init()
    self:SetPos(Vector(100, 100))
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("SizeX", "Int", 100)
    self:AddDataTableEntry("SizeY", "Int", 100)
end

function ENT:Draw()
    local pos = self:GetPos()
    draw.SetColor(PLATFORM_COLOR)
    draw.Rect(pos.x, pos.y, self:GetSizeX(), self:GetSizeY())
end

function ENT:Freeze()
    return false
end
