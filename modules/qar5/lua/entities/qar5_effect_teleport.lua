local TELEPORT_COLOR = Color(0, 155, 255)

function ENT:Spawn()
    if CLIENT then
        for i = 1, 15 do
            local particle = ents.Create("qar5_particle")
            particle:SetPos(self:GetPos() + Vector(math.random()*32-16, math.random()*32-16))
            particle.gravity = True
            particle:SetVel(Vector(math.random()*320-160, math.random()*320-160))
            particle.color = TELEPORT_COLOR
            particle.size_decrease = 16
            ents.SpawnClientside(particle)
        end
        return
    end
    self:Remove()
end