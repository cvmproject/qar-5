function ENT:Init()
    self:SetAutoPhysicsEnabled(false)
end

function ENT:Tick()
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("Owner", "Player", nil)
    self:AddDataTableEntry("Direction", "Int", 1)
end

function ENT:Spawn()
    if not IsRemote() then
        gameController:EmitSound("effects/firenew"..math.random(1, 3))
    end
    if not IsRemote() then
        self:Remove()
    end
end
