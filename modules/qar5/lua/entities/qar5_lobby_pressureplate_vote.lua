function ENT:Draw()
    local pos = self:GetPos()
    local color = self:GetColor()
    if self:GetVotes() == 0 then
        color.r = color.r/2
        color.g = color.g/2
        color.b = color.b/2
    end
    draw.SetColor(color)
    draw.Rect(pos.x, pos.y, self:GetWidth(), 8)

    draw.Text(self:GetMessage(), "TitilliumWeb", 24, pos.x, pos.y + 64, 255, 255, 255, 255)
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("Votes", "Int", 0)
    self:AddDataTableEntry("Width", "Float", 100)
    self:AddDataTableEntry("Color", "Color", Color(255, 100, 0))
    self:AddDataTableEntry("Message", "String", "ERROR")
end

function ENT:Tick()
    if CLIENT then return end
    local left = self:GetPos().x
    local right = left + self:GetWidth()
    local votes = 0
    for _, character in ipairs(ents.GetByType("qar5_character")) do
        local ch_pos = character:GetPos().x
        if ch_pos < right and ch_pos > left then
            votes = votes + 1
        end
    end
    self:SetVotes(votes)
end
