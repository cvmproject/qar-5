function ENT:SetupDataTable()
    self:AddDataTableEntry("Height", "Int", 512)
    self:AddDataTableEntry("Speed", "Int", 256)
    self:AddDataTableEntry("PlatformLength", "Int", 256)
    self:AddDataTableEntry("PlatformThickness", "Int", 16)
    self:AddDataTableEntry("PlatformCount", "Int", 1)
end

function ENT:Init()
    if CLIENT then return end
    self.platforms = {}
end

function ENT:Freeze()
    return false
end

function ENT:Tick(deltaT)
    if CLIENT then return end
    if #self.platforms == 0 then
        for i = 1, self:GetPlatformCount() do
            local platform = ents.Create("qar5_platform")
            platform:SetSizeX(self:GetPlatformLength())
            platform:SetSizeY(self:GetPlatformThickness())
            platform:SetPos(self:GetPos() + Vector(0, self:GetHeight()/self:GetPlatformCount()*(i-1)))
            platform:SetVel(Vector(0, self:GetSpeed()))
            platform:SetPersistent(false)
            ents.Spawn(platform)
            self.platforms[#self.platforms + 1] = platform
        end
    end
    for _, platform in ipairs(self.platforms) do
        --platform:SetPos(platform:GetPos() + Vector(0, self:GetSpeed()*deltaT))
        if platform:GetPos().y > self:GetHeight() then
            platform:SetPos(self:GetPos())
        end
    end
end
