function ENT:Init()
    self.size = 8
    self.size_decrease = 16
    self.color = Color(255, 255, 255)
    self.gravity = false
    self.glow = false
end

function ENT:Tick(delta)
    self.size = self.size - self.size_decrease*delta
    if self.size <= 0 then self:Remove() end
    if self.gravity then self:SetVel(self:GetVel() - Vector(0, 600*delta)) end
end

function ENT:Draw()
    local pos = self:GetPos()
    
    draw.SetColor(self.color)
    if self.glow then
        for i = 0, 10 do
            draw.Rect(
                pos.x - self.size/2 - self.size/10*i,
                pos.y - self.size/2 - self.size/10*i,
                self.size + self.size/5*i,
                self.size + self.size/5*i
            )
        end
    else
        draw.Rect(
            pos.x - self.size/2,
            pos.y - self.size/2,
            self.size,
            self.size
        )
    end
end
