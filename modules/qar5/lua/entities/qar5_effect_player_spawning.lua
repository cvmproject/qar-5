function ENT:Init()
    self:SetPersistent(false)
    if CLIENT then
        self.last_particle_spawn = 0
    end
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("SpawnFinishTime", "Float", CurTime() + 2)
    self:AddDataTableEntry("TargetPlayer", "Player", nil)
end

function ENT:Spawn()
    if not IsRemote() then
        if self:GetTargetPlayer() == nil then
            error("qar5_effect_player_spawning's TargetPlayer is nil!")
            self:Remove()
        end
        gameController:EmitSound("effects/respawn")
    end
end

function ENT:Tick()
    if CLIENT then
        if CurTime() < self:GetSpawnFinishTime() then
            if CurTime() > self.last_particle_spawn + 0.025 then
                local particle = ents.Create("qar5_particle")

                local shiftVector = Vector(math.random()*128-64, math.random()*128-64)

                local time_left = self:GetSpawnFinishTime() - CurTime()

                particle:SetPos(self:GetPos() + shiftVector)
                particle:SetVel(-shiftVector/time_left)
                particle.size_decrease = particle.size/time_left
                particle.color = self:GetTargetPlayer():GetDataEntity():GetColor()
                ents.SpawnClientside(particle)
                self.last_particle_spawn = CurTime()
            end
        end
        return
    end
    if CurTime() > self:GetSpawnFinishTime() then
        local character = ents.Create("qar5_character")
        character:SetPos(self:GetPos())
        character:SetOwner(self:GetTargetPlayer())
        ents.Spawn(character)

        self:GetTargetPlayer():GetDataEntity():SetCharacter(character)
        self:Remove()
    end
end

function ENT:Draw()
    local color = self:GetTargetPlayer():GetDataEntity():GetColor()
    color.a = 20
    draw.SetColor(color)
    local pos = self:GetPos()
    for i = 1, math.random(5, 20) do
        draw.Rect(pos.x-16-i, pos.y-16-i, 32+i*2, 32+i*2)
    end

    draw.SetColor(self:GetTargetPlayer():GetDataEntity():GetColor())
    draw.Rect(pos.x-16, pos.y-16, 32, 32)

    draw.Sprite("players/player", pos.x-16, pos.y-16, 32, 32)

    draw.SetColor(Color(0, 0, 0, (self:GetSpawnFinishTime() - CurTime())/2*255 ))
    draw.Rect(pos.x-16, pos.y-16, 32, 32)
end
