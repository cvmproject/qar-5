function ENT:SetupDataTable()
    self:AddDataTableEntry("On", "Bool", false)
    self:AddDataTableEntry("Message", "String", "Allow connections")
end

function ENT:Draw()
    local image = nil
    if self:GetOn() then
        image = "map_elements/switch_on"
    else
        image = "map_elements/switch_off"
    end
    local pos = self:GetPos()
    draw.Sprite(image, pos.x-32, pos.y, 64, 32)
    draw.Text(self:GetMessage(), "TitilliumWeb", 24, pos.x-50, pos.y + 64, 255, 255, 255, 255)
end

function ENT:Tick()
    if CLIENT then return end
    for _, ent in ipairs(ents.GetInRadius(self:GetPos(), 32)) do
        if ent:GetType() == "qar5_projectile" then
            if self:GetOn() then
                self:EmitSound("effects/map_elements/switch_off")
                self:SetOn(false)
                server.AllowExternalConnections(false)
            else
                self:EmitSound("effects/map_elements/switch_on")
                self:SetOn(true)
                if self:GetMessage() == "Allow connections" then
                    server.AllowExternalConnections(true)
                else
                    net.Start("Qar5Connect")
                    net.Broadcast()
                end
            end
            ent:Remove()
        end
    end
end
