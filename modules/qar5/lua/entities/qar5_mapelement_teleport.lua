local TELEPORT_COLOR = Color(0, 155, 255)
local TELEPORT_COLOR_FADED = Color(0, 155, 255, 80)

function ENT:Draw()
    local pos = self:GetPos()
    draw.SetColor(TELEPORT_COLOR)
    draw.Rect(pos.x-16, pos.y, 32, 8)

    draw.SetColor(TELEPORT_COLOR_FADED)
    local extra_size = math.random()*2 + 3
    draw.Rect(pos.x-16-extra_size, pos.y, 32+extra_size*2, 8+extra_size)

    if self:GetAdminOnly() then
        if LocalPlayer():IsAdmin() then
            draw.Text("Admin only", "TitilliumWeb", 20, pos.x-45, pos.y+64, 0, 255, 0, 255)
        else
            draw.Text("Admin only", "TitilliumWeb", 20, pos.x-45, pos.y+64, 255, 0, 0, 255)
        end
    end
end

function ENT:DrawDebug()
    draw.SetColor(TELEPORT_COLOR_FADED)
    draw.Line(self:GetPos().x, self:GetPos().y, self:GetTargetPos().x, self:GetTargetPos().y, 4)
    draw.Rect(self:GetTargetPos().x-16, self:GetTargetPos().y-16, 32, 32)
end


function ENT:SetupDataTable()
    self:AddDataTableEntry("TargetPos", "Vector", Vector(1124, 32))
    self:AddDataTableEntry("AdminOnly", "Bool", false)
end

function ENT:Init()
    if CLIENT then
        self.last_particle_spawn = 0
    end
end

function ENT:Tick()
    if CLIENT then
        if CurTime() > self.last_particle_spawn + 0.25 then
            self.last_particle_spawn = CurTime()
            local particle = ents.Create("qar5_particle")
            particle:SetPos(self:GetPos() + Vector(math.random()*20-10, 4))
            particle:SetVel(Vector(math.random()*32-16, math.random()*16))
            particle.color = TELEPORT_COLOR
            particle.size_decrease = 4
            ents.SpawnClientside(particle)
        end
    else
        for _, ent in ipairs(ents.GetInRadius(self:GetPos(), 32, 32)) do
            if ent:GetType() ~= "qar5_character" then goto continue end
            if ent:GetVel().y > 300 then
                if not self:GetAdminOnly() or ent:GetOwner():IsAdmin() then
                    local effect_destination = ents.Create("qar5_effect_teleport")
                    effect_destination:SetPos(self:GetTargetPos())
                    ents.Spawn(effect_destination)
                    local effect_origin = ents.Create("qar5_effect_teleport")
                    effect_origin:SetPos(ent:GetPos())
                    ents.Spawn(effect_origin)
                    ent:SetPos(self:GetTargetPos())
                    self:EmitSound("effects/map_elements/teleport")
                else
                    self:EmitSound("effects/map_elements/teleport_denied")
                end
            end
            :: continue ::
        end
    end
end
