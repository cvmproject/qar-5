local defaultColor = Color(255, 255, 255)


function ENT:Init()
end

function ENT:Tick()
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("Color", "Color", defaultColor)
    self:AddDataTableEntry("HitCharacter", "Entity", nil)
end

function ENT:Spawn()
    if not IsRemote() then
        gameController:EmitSound("effects/hitwall")
    end
    if CLIENT then
        for i = 1, 10 do
            local sprinkle_particle = ents.Create("qar5_particle")
            sprinkle_particle:SetPos(self:GetPos())
            sprinkle_particle:SetVel(Vector(math.random()*200-100, math.random()*200-100))
            sprinkle_particle.gravity = true
            sprinkle_particle.color = self:GetColor()
            ents.SpawnClientside(sprinkle_particle)
            if self:GetHitCharacter() == LocalPlayer():GetDataEntity():GetCharacter() then
                input.ControllerHaptic(100, 1)
            end
        end
    end
    if not IsRemote() then
        self:Remove()
    end
end
