function ENT:Draw()
    local pos = self:GetPos()
    local time_left = self:GetTargetTime() - CurTime()
    time_left = math.floor(time_left)
    if time_left < 10 then
        if time_left <= 0 then
            time_left = "00"
        else
            time_left = "0"..time_left
        end
    else
        time_left = ""..time_left
    end


    draw.Text(time_left, "TitilliumWeb", 96, pos.x-45, pos.y, 0, 255, 0, 255)
    local status = self:GetStatus()
    draw.Text(status, "TitilliumWeb", 24, pos.x-#status*5, pos.y+24, 0, 255, 0, 255)
end

function ENT:SetupDataTable()
    self:AddDataTableEntry("TargetTime", "Float", 20)
    self:AddDataTableEntry("Status", "String", "Waiting for at least 2 players...")
end
