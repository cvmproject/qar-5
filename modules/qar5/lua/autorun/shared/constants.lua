CAMERA_CENTER = Vector(400, 300)
WORLD_SIZE = CAMERA_CENTER*2
CHARACTER_COLORS = {
    {r=0, g=0, b=255, a=255},
    {r=255, g=0, b=0, a=255},
    {r=0, g=255, b=0, a=255},
    {r=255, g=255, b=0, a=255}
}
CHARACTER_COLORS_FADED = {}
for id, color in pairs(CHARACTER_COLORS) do
    local new_color = {r=color.r, g=color.g, b=color.b, a=20}
    CHARACTER_COLORS_FADED[id] = new_color
end
-- lua.SetGlobal("CHARACTER_COLORS", CHARACTER_COLORS)
-- lua.SetGlobal("CHARACTER_COLORS_FADED", CHARACTER_COLORS_FADED)
