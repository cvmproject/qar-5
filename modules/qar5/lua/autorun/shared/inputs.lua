hook.Remove("DefaultInputs")

hook.Add("DefineInputs", "Qar5DefaultInputs", function()
    input.Define("qar5_jump", "Bool", false)
    input.Define("qar5_fire", "Bool", false)
    input.Define("qar5_left", "Bool", false)
    input.Define("qar5_right", "Bool", false)
    input.Define("qar5_scoreboard", "Bool", false)

    if SERVER then return end
    input.Bind("qar5_fire", "Key", input.KEY_DOWN)
    input.Bind("qar5_jump", "Key", input.KEY_UP)
    input.Bind("qar5_left", "Key", input.KEY_LEFT)
    input.Bind("qar5_right", "Key", input.KEY_RIGHT)
    input.Bind("qar5_scoreboard", "Key", input.KEY_TAB)


    input.Bind("qar5_fire", "Key", input.KEY_S)
    input.Bind("qar5_jump", "Key", input.KEY_W)
    input.Bind("qar5_left", "Key", input.KEY_A)
    input.Bind("qar5_right", "Key", input.KEY_D)

    input.Bind("qar5_fire", "ControllerButton", input.CONTROLLER_B)
    input.Bind("qar5_jump", "ControllerButton", input.CONTROLLER_A)
    input.Bind("qar5_fire", "ControllerButton", input.CONTROLLER_R_BUMPER)
    input.Bind("qar5_jump", "ControllerButton", input.CONTROLLER_L_BUMPER)
    input.Bind("qar5_left", "ControllerButton", input.CONTROLLER_DPAD_L)
    input.Bind("qar5_right", "ControllerButton", input.CONTROLLER_DPAD_R)
    input.Bind("qar5_scoreboard", "ControllerButton", input.CONTROLLER_Y)
end)
