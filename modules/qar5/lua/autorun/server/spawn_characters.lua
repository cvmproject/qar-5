local characters = {}

local colors = {Color(0, 255, 0), Color(255, 255, 0), Color(255, 0, 0), Color(0, 0, 255)}

local last_connection_time = 100

hook.Add("PlayerConnected", "SpawnCharacters", function(ply)
    -- local character = SpawnCharacter(ply)
    last_connection_time = CurTime()
    ply:GetDataEntity():SetColor(colors[1])
    table.remove(colors, 1)
    if qar5_gamemode.GetActiveGamemode() == nil then
        print("No gamemode, spawning character right now")
        SpawnCharacterFor(ply)
    end
end)

hook.Add("Think", "WaitForPlayers", function()
    if qar5_gamemode.GetActiveGamemode() == nil then return end
    if CurTime() - last_connection_time > 2 then
        for _, ply in ipairs(player.GetAllActive()) do
            if ply:GetState() ~= "Active" then return end
        end
        hook.CallEvent("Qar5MatchBegin")
        hook.Remove("WaitForPlayers")
    end
end)

hook.Add("Qar5MatchBegin", "SpawnCharacters", function()
    print("QAR5 MATCH BEGINNING!")
    for _, ply in ipairs(player.GetAllActive()) do
        SpawnCharacterFor(ply)
    end
end)

hook.Add("PlayerDisconnected", "DespawnCharacters", function(ply)
    print("Player disconnected, removing character")
    if ply:GetDataEntity():GetCharacter() ~= nil then
        ply:GetDataEntity():GetCharacter():Remove()
    end
end)


function SpawnCharacterFor(ply)
    print("Choosing spawn poing")
    local spawn_points = ents.GetByType("qar5_spawn_point")
    local chosen_pos = Vector(100, 100)
    local free_spawn_points = {}
    local spawn_point_counter = 1
    if #spawn_points > 0 then
        for _, spawn_point in ipairs(spawn_points) do
            local point_suitable = true
            for _, character in ipairs(ents.GetByType("qar5_character")) do
                if character:GetPos():Distance(spawn_point:GetPos()) < 200 then
                    point_suitable = false
                    break
                end
            end

            if point_suitable then
                free_spawn_points[spawn_point_counter] = spawn_point
                spawn_point_counter = spawn_point_counter + 1
            end
        end

        if #free_spawn_points == 0 then free_spawn_points = spawn_points end

        chosen_pos = free_spawn_points[math.random(#free_spawn_points)]:GetPos()
    end

    local spawn_effect = ents.Create("qar5_effect_player_spawning")
    spawn_effect:SetPos(chosen_pos)
    spawn_effect:SetTargetPlayer(ply)
    ply:GetDataEntity():SetRespawnPoint(chosen_pos)
    -- spawn_effect:SetColor(ply:GetDataEntity():GetColor())
    -- local freeCharacterID = #characters + 1
    -- character:SetQarCharacterID(freeCharacterID)
    -- characters[freeCharacterID] = character
    ents.Spawn(spawn_effect)
    return spawn_effect
end
