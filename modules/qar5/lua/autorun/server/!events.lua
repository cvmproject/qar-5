hook.DefineEvent("Qar5TakeDamage") -- Called when a character takes damage (character, damage, attacking_player)
hook.DefineEvent("Qar5CharacterSpawned") -- Called right after a character gets spawned (character)
hook.DefineEvent("Qar5CharacterDeath") -- Called right before a character begins its death sequence (character, killer_player)
hook.DefineEvent("Qar5CharacterDestroyed") -- Called right before a character gets removed (character)
hook.DefineEvent("Qar5MatchBegin") -- Called when all players have connected, characters should spawn and the round timer should begin
hook.DefineEvent("Qar5MatchEnd") -- Called by the gamemode when someone wins, freezes input

