local state = "WaitingForTwo"
local time = -10

local voters = {}
local chosen_gamemode = nil
local chosen_map = nil

local x_start = 16
local x_end = 1008
local x_size = x_end - x_start

local color_sequence = {
    Color(255, 0, 0),
    Color(0, 0, 255),
    Color(0, 255, 0),
    Color(255, 255, 0),
    Color(0, 255, 255),
    Color(255, 0, 255),
    Color(255, 175, 0),
    Color(100, 255, 0)
}

local timer_entity = nil

hook.Add("WorldStarted", "InitLobby", function()
    if server.GetServerParams().map ~= "qar5_lobby" then
        return
    end
    timer_entity = ents.Create("qar5_lobby_timer")
    timer_entity:SetPos(Vector(x_start + x_size/2, 400))
    timer_entity:SetTargetTime(time)
    ents.Spawn(timer_entity)
end)

local function StartVote(vote_time, options, status)
    time = CurTime() + vote_time
    timer_entity:SetTargetTime(time)
    timer_entity:SetStatus(status)
    for i, option in ipairs(options) do
        local voter = ents.Create("qar5_lobby_pressureplate_vote")
        voter:SetWidth(x_size/#options)
        voter:SetMessage(option.name)
        voter.result = option
        voter:SetColor(color_sequence[i])
        voter:SetPos(Vector(x_start + (i-1)*x_size/#options, 16))
        ents.Spawn(voter)
        voters[#voters + 1] = voter
    end
end

local function EndVote()
    local max_votes = 0
    local winner = nil
    for _, voter in ipairs(voters) do
        if voter:GetVotes() > max_votes then
            max_votes = voter:GetVotes()
            winner = voter.result
        end
        voter:Remove()
    end
    voters = {}
    return winner
end

hook.Add("Tick", "UpdateLobbyScript", function()
    if server.GetServerParams().map ~= "qar5_lobby" then
        hook.Remove("UpdateLobbyScript")
        return
    end
    if state == "WaitingForTwo" then
        if #player.GetAllActive() > 1 then
            state = "Waiting"
            time = CurTime() + 10
            timer_entity:SetTargetTime(time)
            timer_entity:SetStatus("Waiting for players")
        end
    elseif state == "Waiting" then
        if CurTime() > time then
            state = "VotingGamemode"
            StartVote(5, qar5_gamemode.ListGamemodes(), "Voting for gamemode")
        end
    elseif state == "VotingGamemode" then
        if CurTime() > time then
            state = "VotingMap"
            chosen_gamemode = EndVote()
            StartVote(10, qar5_gamemode.ListMapsForGamemode(chosen_gamemode.id), "Voting for map")
        end
    elseif state == "VotingMap" then
        if CurTime() > time then
            chosen_map = EndVote()
            state = "StartingMap"
            time = CurTime() + 5.9
            timer_entity:SetTargetTime(time)
            timer_entity:SetStatus("Starting "..chosen_gamemode.name.." on "..chosen_map.name..", get ready...")
            --StartVote(10, qar5_gamemode.ListAllMapsForGamemode(chosen_gamemode.id), "Voting for map")
        end
    elseif state == "StartingMap" then
        if CurTime() > time then
            local params = server.GetServerParams()
            params.gamemode = chosen_gamemode.id
            params.map = chosen_map.path
            server.Reload()
            state = "ERROR"
        end
    end
end)

hook.Add("Qar5MatchEnd", "DefaultEndBehavior", function()
    for _, character in ipairs(ents.GetByType("qar5_character")) do
        character:Remove()
    end
    net.Start("Qar5MatchEnd")
    net.Broadcast()
    timer.Simple(10, function()
        local params = server.GetServerParams()
        params.map = "qar5_lobby"
        params.gamemode = nil
        server.Reload()
    end)
end)

hook.Add("WorldStarted", "LoadLobby", function()
    local params = server.GetServerParams()
    if params.map == nil then
        params.map = "qar5_lobby"
        params.gamemode = nil
        server.Reload()
    end
end)
