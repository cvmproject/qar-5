hook.Add("Connected", "AndroidControls", function()
    if not ANDROID then return end
    left_button = cvm_ui.CreateElement("Button")
    left_button:SetColor(Color(200, 200, 200, 50))
    left_button:SetHighlightColor(Color(200, 200, 200, 50))
    left_button:SetPressedColor(Color(200, 200, 200, 70))
    left_button:SetPos(0, 0)
    left_button:SetSize(draw.UIScreenHeight()/4, draw.UIScreenHeight()/4)
    left_button.on_press = function()
        LocalPlayer():SetInput("qar5_left", true)
    end
    left_button.on_release = function()
        LocalPlayer():SetInput("qar5_left", false)
    end

    right_button = cvm_ui.CreateElement("Button")
    right_button:SetColor(Color(200, 200, 200, 50))
    right_button:SetHighlightColor(Color(200, 200, 200, 50))
    right_button:SetPressedColor(Color(200, 200, 200, 70))
    right_button:SetPos(draw.UIScreenHeight()/4, 0)
    right_button:SetSize(draw.UIScreenHeight()/4, draw.UIScreenHeight()/4)
    right_button.on_press = function()
        LocalPlayer():SetInput("qar5_right", true)
    end
    right_button.on_release = function()
        LocalPlayer():SetInput("qar5_right", false)
    end

    jump_button = cvm_ui.CreateElement("Button")
    jump_button:SetColor(Color(150, 250, 150, 50))
    jump_button:SetHighlightColor(Color(150, 250, 150, 50))
    jump_button:SetPressedColor(Color(150, 250, 150, 70))
    jump_button:SetPos(draw.UIScreenWidth()-draw.UIScreenHeight()/2, 0)
    jump_button:SetSize(draw.UIScreenHeight()/4, draw.UIScreenHeight()/4)
    jump_button.on_press = function()
        LocalPlayer():SetInput("qar5_jump", true)
    end
    jump_button.on_release = function()
        LocalPlayer():SetInput("qar5_jump", false)
    end

    fire_button = cvm_ui.CreateElement("Button")
    fire_button:SetColor(Color(250, 150, 150, 50))
    fire_button:SetHighlightColor(Color(250, 150, 150, 50))
    fire_button:SetPressedColor(Color(250, 150, 150, 70))
    fire_button:SetPos(draw.UIScreenWidth()-draw.UIScreenHeight()/4, 0)
    fire_button:SetSize(draw.UIScreenHeight()/4, draw.UIScreenHeight()/4)
    fire_button.on_press = function()
        LocalPlayer():SetInput("qar5_fire", true)
    end
    fire_button.on_release = function()
        LocalPlayer():SetInput("qar5_fire", false)
    end
end)
