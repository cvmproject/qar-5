hook.Add("CameraFocusUpdate", "FollowCharacter", function()
    if client.IsConnected() then
        local character = LocalPlayer():GetDataEntity():GetCharacter()
        if character ~= nil then
            camera_controller.SetCameraFocus(character:GetPos())
        else
            local respawn_point = LocalPlayer():GetDataEntity():GetRespawnPoint()
            if respawn_point ~= nil then
                camera_controller.SetCameraFocus(respawn_point)
            end
        end
    end
end)
