WHITE = Color(255, 255, 255)

local ui_initialized = false

hook.Add("GameStart", "InitQAR5Game", function()
    --if not ANDROID then
    --    client.StartListenServer(20480, {map="qar5_lobby"})
    --else
    --    client.Connect("complover116.com", 20480)
    --end
    sound.PlayMusic("CVM_SP_DARKMASTER", 0.5)

    hook.Add("UIDraw", "IntroDraw", function()
        draw.SetColor(WHITE)
        local sprite_name = nil
        if sound.GetMusicPos("CVM_SP_DARKMASTER") < 2/1.5 then
            sprite_name = "intro/intro1"
        elseif sound.GetMusicPos("CVM_SP_DARKMASTER") < 4/1.5 then
            sprite_name = "intro/intro2"
        elseif sound.GetMusicPos("CVM_SP_DARKMASTER") < 6/1.5 then
            sprite_name = "intro/intro3"
        end
        if sprite_name ~= nil then
            draw.Sprite(sprite_name, 0, 0, 1280, 1024)
        end
    
        if sound.GetMusicPos("CVM_SP_DARKMASTER") > 6/1.5 then
            draw.Sprite("intro/title1", 0, 0, 1280, 1024)
        end
        if sound.GetMusicPos("CVM_SP_DARKMASTER") > 6/1.5 + 0.5 then
            draw.Sprite("intro/title2", 0, 0, 1280, 1024)
        end
        if sound.GetMusicPos("CVM_SP_DARKMASTER") > 6/1.5 + 0.85 then
            draw.Sprite("intro/title3", 0, 0, 1280, 1024)
        end
        if sound.GetMusicPos("CVM_SP_DARKMASTER") > 6/1.5 + 1.0 then
            draw.Sprite("intro/title5", 0, 0, 1280, 1024)
        end
        if sound.GetMusicPos("CVM_SP_DARKMASTER") > 8/1.5 then
            if not ui_initialized then
                ui_initialized = true
                InitMainMenu()
            end
    
    
            draw.Sprite("intro/title4", 0, 0, 1280, 1024)
            for tit = 1, 5 do
                for i = 1, math.random(0, 1) do
                    draw.Sprite("intro/title"..tit.."-blur", 0, 0, 1280, 1024)
                end
            end
        end
    end)
end)
