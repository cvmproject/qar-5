function InitMainMenu()
    local main_screen = cvm_ui.CreateElement("Panel")
    local start_screen = cvm_ui.CreateElement("Panel")
    start_screen:Disable()
    local join_screen = cvm_ui.CreateElement("Panel")
    join_screen:Disable()

    local quit_button = cvm_ui.CreateElement("Button")
    quit_button:SetText("Quit")
    quit_button:SetPos(1280/2-200, 30)
    quit_button:SetSize(400, 100)
    quit_button.on_confirm = function()
        client.Exit()
    end

    main_screen:Add(quit_button)

    local start_button = cvm_ui.CreateElement("Button")
    start_button:SetText("Start Game")
    start_button:SetPos(1280/2-400, 500)
    start_button:SetSize(800, 130)
    start_button.on_confirm = function()
        main_screen:Disable()
        start_screen:Enable()
    end

    main_screen:Add(start_button)

    local join_button = cvm_ui.CreateElement("Button")
    join_button:SetText("Join Game")
    join_button:SetPos(1280/2-400, 350)
    join_button:SetSize(800, 130)
    join_button.on_confirm = function()
        main_screen:Disable()
        join_screen:Enable()
    end

    main_screen:Add(join_button)

    local settings_button = cvm_ui.CreateElement("Button")
    settings_button:SetText("Settings")
    settings_button:SetPos(1280/2-400, 200)
    settings_button:SetSize(800, 130)
    settings_button.on_confirm = function()
        client.Exit()
    end

    main_screen:Add(settings_button)

    
    cvm_ui.AddToRoot(main_screen)

    local start_screen_back_button = cvm_ui.CreateElement("Button")
    start_screen_back_button:SetText("Back")
    start_screen_back_button:SetPos(1280/2-610, 30)
    start_screen_back_button:SetSize(400, 100)
    start_screen_back_button.on_confirm = function()
        main_screen:Enable()
        start_screen:Disable()
    end
    start_screen:Add(start_screen_back_button)

    local enable_multiplayer = cvm_ui.CreateElement("Checkbox")
    enable_multiplayer:SetPos(100, 200)
    enable_multiplayer:SetSize(50, 50)
    enable_multiplayer:SetText("Enable multiplayer")
    enable_multiplayer.checked = true
    
    start_screen:Add(enable_multiplayer)

    local enable_bots = cvm_ui.CreateElement("Checkbox")
    enable_bots:SetPos(550, 200)
    enable_bots:SetSize(50, 50)
    enable_bots:SetText("Enable bots [NOT IMPLEMENTED]")
    
    start_screen:Add(enable_bots)


    local start_screen_start_button = cvm_ui.CreateElement("Button")
    start_screen_start_button:SetText("START")
    start_screen_start_button:SetPos(1280/2-190, 30)
    start_screen_start_button:SetSize(800, 100)
    start_screen_start_button:SetColor(10, 250, 10)
    start_screen_start_button.on_confirm = function()
        client.StartListenServer(20480, {map="dm_reload_arena", gamemode="dm"})
    end
    start_screen:Add(start_screen_start_button)


    cvm_ui.AddToRoot(start_screen)


    local join_screen_back_button = cvm_ui.CreateElement("Button")
    join_screen_back_button:SetText("Back")
    join_screen_back_button:SetPos(1280/2-200, 30)
    join_screen_back_button:SetSize(400, 100)
    join_screen_back_button.on_confirm = function()
        main_screen:Enable()
        join_screen:Disable()
    end
    join_screen:Add(join_screen_back_button)


    local join_lan_button = cvm_ui.CreateElement("Button")
    join_lan_button:SetText("Join LAN Game")
    join_lan_button:SetPos(1280/2-400, 500)
    join_lan_button:SetSize(800, 130)
    join_lan_button.on_confirm = function()
        client.AutoConnectLan(20480)
    end

    join_screen:Add(join_lan_button)


    local join_official_button = cvm_ui.CreateElement("Button")
    join_official_button:SetText("Official Server")
    join_official_button:SetPos(1280/2-400, 350)
    join_official_button:SetSize(800, 130)
    join_official_button.on_confirm = function()
        client.Connect("complover116.com", 20480)
    end

    join_screen:Add(join_official_button)


    cvm_ui.AddToRoot(join_screen)
end
