local force_show_scoreboard = false

net.Receive("Qar5MatchEnd", function()
    force_show_scoreboard = true
end)


hook.Add("UIDraw", "DrawScoreBoard", function()
    if not client.IsConnected() then return end

    local local_player = LocalPlayer()


    if local_player:GetInput("qar5_scoreboard") or force_show_scoreboard then
        local scoreboard_width = 1280
        local scoreboard_x = 0
        local scoreboard_height = 1024/2
        local scoreboard_y = 1024/4

        local scoreboard_row_height = 40

        scoreboard_height = math.floor(scoreboard_height/scoreboard_row_height)*scoreboard_row_height

        draw.SetColor(Color(100, 100, 100, 177))
        draw.Rect(scoreboard_x, scoreboard_y, scoreboard_width, scoreboard_height)

        local table_contents = {
            {
                "Name",
                "Ping",
                "Color",
                "Score"
            }
        }

        for _, ply in ipairs(player.GetAllActive()) do
            table_contents[#table_contents + 1] = {
                ply:GetUsername(),
                math.floor(ply:GetPing()).."ms",
                "#COLOR",
                ply:GetDataEntity():GetScore(),
                player=ply
            }
        end

        for i = scoreboard_height/scoreboard_row_height - 1, 0, -1  do
            local true_row = scoreboard_height/scoreboard_row_height - i

            if math.fmod(i, 2) == 0 then
                draw.SetColor(Color(100, 100, 100, 177))
                draw.Rect(scoreboard_x, scoreboard_y + scoreboard_row_height*i, scoreboard_width, scoreboard_row_height)
            end
            if #table_contents >= true_row then
                row_data = table_contents[true_row]
                for posi, row_data_element in ipairs(row_data) do
                    if row_data_element == "#COLOR" then
                        draw.SetColor(Color(0, 0, 0))
                        draw.Rect(
                                scoreboard_x + scoreboard_width/(#row_data)*(posi - 1),
                                scoreboard_y + scoreboard_row_height*(i),
                                scoreboard_row_height,
                                scoreboard_row_height
                        )
                        draw.SetColor(row_data.player:GetDataEntity():GetColor())
                        draw.Rect(
                                scoreboard_x + scoreboard_width/(#row_data)*(posi - 1) + scoreboard_row_height*0.1,
                                scoreboard_y + scoreboard_row_height*(i+0.1),
                                scoreboard_row_height*0.8,
                                scoreboard_row_height*0.8
                        )
                    else
                        draw.Text(row_data_element, "TitilliumWeb", scoreboard_row_height, scoreboard_x + scoreboard_width/(#row_data)*(posi - 1), scoreboard_y + scoreboard_row_height*(i+0.9),  255, 255, 255, 255, false)
                    end
                end
            end
        end
    end
end)
