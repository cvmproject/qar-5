function Intersects(pos1, size1, pos2, size2)
    return pos2.x < pos1.x + size1.x and pos2.x + size2.x > pos1.x and pos2.y < pos1.y + size1.y and pos2.y + size2.y > pos1.y
end

function Contains(pos1, size1, pos2)
    return pos2.x > pos1.x and pos2.x < pos1.x + size1.x and pos2.y > pos1.y and pos2.y < pos1.y + size1.y
end

_RECTANGLE = {}

function _RECTANGLE:Intersects(rect2)
    return Intersects(self.pos, self.size, rect2.pos, rect2.size)
end

function _RECTANGLE:Contains(pos)
    return Contains(self.pos, self.size, pos)
end

function Rectangle(pos, size)
    local rectangle = {
        pos=pos,
        size=size
    }

    setmetatable(rectangle, {__index=_RECTANGLE})
    return rectangle
end
