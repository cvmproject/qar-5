rm -r out-windows64

java -jar packr.jar \
     --platform windows64 \
     --jdk JRE.zip \
     --useZgcIfSupportedOs \
     --executable QAR-5 \
     --classpath .cvm/engine/CVMEngine.jar \
     --mainclass com.complover116.cvmproject.cvmengine.desktop.DesktopLauncher \
     --vmargs Xmx1G \
     --resources modules \
     --output out-windows64