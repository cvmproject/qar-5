FROM openjdk:11
RUN mkdir -p /app/build
COPY . /app/build
WORKDIR /app/build
RUN java -jar CVMConfigurator.jar -desktop_dist
RUN cp CVMProject.zip ..
WORKDIR /app
RUN rm -r build
RUN unzip CVMProject.zip
RUN rm CVMProject.zip
CMD ["java", "-jar", "CVMEngine.jar", "-dedicated"]
